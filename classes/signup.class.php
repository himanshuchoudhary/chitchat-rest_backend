<?php
/**
* Signup class - Methods for registering new user
* @author Himanshu Choudhary
*/
class Signup
{
	public function __construct() 
	{
		return TRUE;
	}

	private function validateRegisterParams($params)
	{
		if(isset($params['email']) && !empty($params['email']) &&
			isset($params['name']) && !empty($params['name']) &&
			isset($params['password']) && !empty($params['password'])) {
			return TRUE;
		}
		else {
			return FALSE;
		}
	}

	public function registerUser()
	{
		Global $db,$app;

		$params = $app->request->post();
		$response = [];
		
		if($this->validateRegisterParams($params)){
			$query = "INSERT INTO users (email,name,password) VALUES ('".$params['email']."','".$params['name']."','".$params['password']."')";
			$db->executeQuery($query);
			if($db->error){
				$response['success'] = 'false';
				$response['status'] = 302;
				$response['message'] = 'Error in inserting into database. Error : '.$db->err_mess;
				$response['data'] = NULL;
			}
			else {
				$generated_uid = $db->lastInsertId();
				
				$response['success'] = 'true';
				$response['status'] = 200;
				$response['message'] = 'Successfully registered';
				$response['data'] = array(
					'id' => $generated_uid,
					'name' => $params['name'],
					'email' => $params['email'],
					'query' => $query
				);
			}
		}
		else {
			$response['success'] = 'false';
			$response['status'] = 400;
			$response['message'] = 'Given parameters are invalid';
			$response['data'] = NULL;
		}

		echo json_encode($response);
	}
}