<?php

/**
 * Database class - For connection and executing queries
 *
 * @author Himanshu Choudhary
*/

class Database
{
	private $host      = DB_HOST;
	private $user      = DB_USER;
	private $pass      = DB_PASS;
	private $dbname    = DB_NAME;
	private $conn;

	public $query  		= NULL;
	public $result		= NULL;
	public $error		= NULL;
	public $err_mess	= NULL;

	public function __construct() 
	{	
		$this->conn = new mysqli($this->host, $this->user, $this->pass, $this->dbname);

		if ($this->conn->connect_error){
			$this->error = TRUE;
			$this->err_mess = $this->conn->connect_error();
	  	}
	  	else {
	  		$this->error = FALSE;
	  	}
	}

	public function executeQuery($query)
	{
		$this->query = $query;
		$res = $this->conn->query($query);
		if ($res){
			$this->error = FALSE;
			$this->result = $res;
		}
		else {
			$this->error = TRUE;
			$this->err_mess = $this->conn->error;
			$this->result = NULL;
		}
	}

	public function fetchAll()
	{
		return $this->result->fetch_all();
	}

	public function numOfRows()
	{
		$num = $this->result->num_rows;
		($num) ? $num : 0; 
	}

	public function lastInsertId()
	{
		return $this->conn->insert_id;
	}

	public function closeConnection()
	{
		$this->conn->close();
	}

}