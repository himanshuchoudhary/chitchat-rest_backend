<?php

    include 'includes/config.php';
    include 'includes/app_config.php';

    $app->get('/hello/:name', function ($name) {
        echo "Hello, $name";
    });

    $app->post('/user/register','\Signup:registerUser');

    $app->run();
