<?php
	/**
	 * other required files - call functions
	 *
	 * @author Himanshu Choudhary
	 * @version 1.0 
	*/

	// Include classes
	include 'classes/db.class.php';
	include 'classes/signup.class.php';


	require 'Slim/Slim.php';

	\Slim\Slim::registerAutoloader();

	$app = new \Slim\Slim();

	$db = new Database();

?>